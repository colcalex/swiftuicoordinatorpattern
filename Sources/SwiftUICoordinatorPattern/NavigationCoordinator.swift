//
//  NavigationCoordinator.swift
//  
//
//  Created by Alex Ioja-Yang on 09/09/2021.
//

import SwiftUI

/// Protocol allowing to control navigation
public protocol NavigationCoordinator: AnyObject {
    var view: NavigationCoordinatorView? { get }
}

