//
//  NextNavigationCoordinator.swift
//  
//
//  Created by Alex Ioja-Yang on 09/09/2021.
//

import Foundation

/// Reference type wrapper allowing next coordinator to be changed inside value type objects
public class NextNavigationCoordinator {
    
    private var next: NavigationCoordinator?
    
    public init() { }
    
    public func setNext(coordinator: NavigationCoordinator) {
        self.next = coordinator
    }
    
    public func removeNextCoordinator() {
        self.next = nil
    }
    
    public func get() -> NavigationCoordinator? {
        next
    }
    
}
