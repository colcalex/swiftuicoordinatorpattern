//
//  NavigationRootView.swift
//  
//
//  Created by Alex Ioja-Yang on 09/09/2021.
//

import SwiftUI

/// Root View Enabling coordinator  driven navigation
public struct NavigationRootView: View {
    
    public init(rootCoordinator: NavigationCoordinator) {
        coordinator = rootCoordinator
    }
    
    private let coordinator: NavigationCoordinator
    
    public var body: some View {
        NavigationView {
            coordinator.view
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}
