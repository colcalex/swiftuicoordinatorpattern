//
//  ObservableWrapper.swift
//  
//
//  Created by Alex Ioja-Yang on 09/09/2021.
//

import Foundation

class ObservableWrapper<T>: ObservableObject {
    
    @Published var value: T
    
    init(defaultValue: T) {
        value = defaultValue
    }
    
}

