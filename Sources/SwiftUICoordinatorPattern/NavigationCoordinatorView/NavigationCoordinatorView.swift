//
//  NavigationCoordinatorView.swift
//  
//
//  Created by Alex Ioja-Yang on 09/09/2021.
//

import SwiftUI

/// Navigatable view for a NavigationCoordinator, wrapping the navigation behaviour and enabling programmatic navigation
public struct NavigationCoordinatorView: View {
    
    public var isPresentingChild: Bool {
        showNext.value
    }
    
    @ObservedObject private var showNext = ObservableWrapper(defaultValue: false)
    private var next: NextNavigationCoordinator = NextNavigationCoordinator()
    private let content: AnyView
    private let configuration: NavigationCoordinatorViewConfiguration
    
    public init(configuration: NavigationCoordinatorViewConfiguration, _ content: @escaping (() -> AnyView)) {
        self.content = content()
        self.configuration = configuration
    }
    
    public var body: some View {
        ZStack {
            NavigationLink(
                destination: next.get()?.view,
                isActive: $showNext.value,
                label: {
                    EmptyView()
                })
            content
                .onAppear {
                    next.removeNextCoordinator()
                }
        }
        .navigationBarColor(configuration.backgroundColor, foregroundColor: configuration.foregroundColor)
    }
    
    /// Present the view and hold the reference to the  provided navigation coordinator in the memory of this coordinator
    public func show(_ next: NavigationCoordinator) {
        self.next.setNext(coordinator: next)
        showNext.value = true
    }
}
