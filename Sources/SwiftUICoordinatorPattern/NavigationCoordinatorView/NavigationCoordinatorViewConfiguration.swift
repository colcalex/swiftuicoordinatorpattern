//
//  NavigationCoordinatorViewConfiguration.swift
//  
//
//  Created by Alex Ioja-Yang on 09/09/2021.
//

import UIKit

/// Configuration object for the background of the navigation controller bar
public struct NavigationCoordinatorViewConfiguration {
    
    static public func invisible() -> Self {
        NavigationCoordinatorViewConfiguration(backgroundColor: .clear, foregroundColor: .clear)
    }
    
    let backgroundColor: UIColor
    let foregroundColor: UIColor
    
    public init(backgroundColor: UIColor, foregroundColor: UIColor) {
        self.backgroundColor = backgroundColor
        self.foregroundColor = foregroundColor
    }
}
