# SwiftUICoordinatorPattern 

Pattern enabling the use of coordinators for navigation between views

Whilst to some extent it achieves the goal, the use of AnyView to wrap the coordinator views is not ideal and leads to a loss of animation between state changes. Also, not unit tested at the moment.

Use at own peril, but feel free to open suggestions and propose changes!
