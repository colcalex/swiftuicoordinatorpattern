// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftUICoordinatorPattern",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "SwiftUICoordinatorPattern",
            targets: ["SwiftUICoordinatorPattern"]),
    ],
    targets: [
        .target(
            name: "SwiftUICoordinatorPattern",
            dependencies: []),
        .testTarget(
            name: "SwiftUICoordinatorPatternTests",
            dependencies: ["SwiftUICoordinatorPattern"]),
    ]
)
